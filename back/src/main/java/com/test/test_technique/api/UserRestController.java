package com.test.test_technique.api;

import com.test.test_technique.domain.Profile;
import com.test.test_technique.domain.User;
import com.test.test_technique.repository.ProfileRepository;
import com.test.test_technique.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

@RestController
@RequestMapping(path = "/api")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class UserRestController {

    private final Path uploadsPath = Paths.get("");

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ProfileRepository profileRepository;

    @RequestMapping(path = "/users",method = RequestMethod.GET)
    public ResponseEntity getAllUsers(){
        List<User> users = this.userRepository.findAll();
        return new ResponseEntity(users,HttpStatus.OK);
    }

    @RequestMapping(path = "/profiles",method = RequestMethod.GET)
    public ResponseEntity getAllProfiles(){
        List<Profile> profiles = this.profileRepository.findAll();
        return new ResponseEntity(profiles,HttpStatus.OK);
    }

    @PostMapping("/users")
    public User createUser(@RequestBody User newUser) {
        return this.userRepository.save(newUser);
    }


    @PostMapping("/create")
    public String createUserWithImage(@RequestPart("photo") MultipartFile file ) throws IOException {

        String fileName =  file.getOriginalFilename();
        System.out.println("============== " + fileName);
        Files.copy(file.getInputStream(),this.uploadsPath.resolve(fileName));
        return "https://theimag.org/wp-content/uploads/2015/01/user-icon-png-person-user-profile-icon-20-300x300.png";
    }


}
